import 'package:flutter/material.dart';
import 'package:audioplayers/audio_cache.dart';

void main() => runApp(XylophoneApp());

class XylophoneApp extends StatelessWidget {
  void plays(int sounds) {
    final player = AudioCache();
    player.play('note$sounds.wav');
  }

  Expanded buildKey(Color color,int sounds ) {
  return  Expanded(
      child: FlatButton(
        color: color,
        onPressed: () {
          plays(sounds);
        },
        child: Text('press me !'),
      ),
    );
  
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.black,
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
           buildKey(Colors.yellow,1),
           buildKey(Colors.red,2),
           buildKey(Colors.brown,3),
           buildKey(Colors.teal,4),
           buildKey(Colors.blue,5),
           buildKey(Colors.purple,6),
           buildKey(Colors.indigo,7),
           
          ],
        ),
      ),
    );
  }
}
